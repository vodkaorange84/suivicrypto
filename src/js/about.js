const ipc = require('electron').ipcRenderer;	// importe ipc côté script de rendu
const remote = require('electron').remote;
const shell = require ('electron').shell;
window.$ = window.jquery = require('jquery');

// fonctions en provenance du main process
var getVersion = remote.require('./main').getVersion;

$('#version').text(getVersion());	// version du logiciel lue dans le package.json

$(document).on('click', 'a', (e) => {
	e.preventDefault();
	//shell.openExternal(e.currentTarget.href);
	ipc.send('open_link', {
		'url': e.currentTarget.href
	});//*/
});