const ipc = require('electron').ipcRenderer;
window.$ = window.jquery = require('jquery');
const remote = require('electron').remote;

var list = $("#grid_currencies");
var msg = $('#msg_currency');


updateGrid();

function addCurrency() {
	var val = $('#value_currency').val();
	ipc.send('add_currency', {
		'iso_code': val
	});
}

// traitement du clic sur le bouton d'ajout d'une monnaie
$('#add_currency').on('click', () => {
	addCurrency();
});

// Appui sur la touche Entrée pour l'ajout d'une monnaie
$('#add_currency').on('keypress', (e) => {
	if(e.keyCode && (e.keyCode !== 13)) {
		return;
	}
	addCurrency();
});

// traitement du retour d'ajout d'une monnaie
ipc.on('return_add_currency', (event, result, error) => {
	switch(result) {
		case '0':
			msg.html('<img src="../assets/checked.png" /> La devise a été ajoutée');
			setTimeout(function() { // reset après 5 secondes
				msg.html('');
			}, 5000);
			break;
		default:
			msg.html('<img src="../assets/error.png" /> ' + error);
			break;
	}
	updateGrid();		// mise à jour de la liste
});


// traitement du clic sur un des boutons de suppression
$(document).off().on('click', 'img.delete', (event) => {
	ipc.send('delete_currency', {
		iso_code: event.currentTarget.id
	});

	
});
ipc.on('return_delete_currency', (event, result) => {
	if(result == '1') {
		msg.html('<img src="../assets/checked.png" /> La devise a été supprimée');
		setTimeout(function() { // reset après 5 secondes
			msg.html('');
		}, 5000);
	}
	else {
		msg.html('<img src="../assets/error.png" /> Erreur de suppression');
	}
	updateGrid();
	
});

// fermeture de la fenetre
$('#close').on('click', () => {
	var win = remote.getCurrentWindow();
	win.close();
});


/** 
* updateGrid()
* Met à jour la liste des devises (envoi d'un message au main)
*/
function updateGrid() {
	// demande de la liste
	ipc.send('get_currencies', {
		values: false
	});
	
}
ipc.on('return_get_currencies', (event, currenciesList) => {
	list.empty();		// reset de la liste
	var currencies = JSON.parse(currenciesList);
	for(var curr in currencies) {	// curr => clé = Code ISO
		var li = '<li><img src="../assets/delete.png" class="delete" id="' + curr + '" title="Supprimer la monnaie ' + curr + '"/> ' + curr + ' <span>' + currencies[curr][CURRENCY_NAME] + '</span></li>';
		list.append(li);
	}
});

