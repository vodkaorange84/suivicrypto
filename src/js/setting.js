const ipc = require('electron').ipcRenderer;	// importe ipc côté script de rendu
const remote = require('electron').remote;
window.$ = window.jquery = require('jquery');


updateParams();


// Sauvegarde des paramètres
$('#save_params').on('click', () => {
	var state = $('#state_return');
	var retImg, retMsg;
	$('#loader').removeClass('hidden');
	if($('#api_key').val() == '') {
		$('#api_key').css('background-color', 'orange');
		retImg = 'warning';
		retMsg = 'La clé API peut être nécessaire';
		//return;
	}
	if($('#refresh').val() == '') {
		$('#refresh').attr('title', 'Ce champ doit comporter une valeur');
		$('#refresh').css('background-color', 'red');
		retImg = 'error';
		retMsg = 'Un ou plusieurs champs sont vides';
		return;
	}
	if($('#select_start_currency :selected').val() == '') {
		$('#select_start_currency :selected').attr('title', 'Ce champ doit comporter une valeur');
		$('#select_start_currency :selected').css('background-color', 'red');
		retImg = 'error';
		retMsg = 'Un ou plusieurs champs sont vides';
		return;
	}
	ipc.send('save_params', {
		'api_key': $('#api_key').val(),
		'refresh': $('#refresh').val(),
		'select_start_currency': $('#select_start_currency :selected').val()
	});
	
});
ipc.on('return_save_params', (event, ret, _retMsg) => {
	if(ret) {
		updateParams();
		retImg = 'checked';
		retMsg = 'Enregistrement OK<br/>Les modifications seront prises en compte lors du redémarrage.<br/>';
		retMsg += 'Vous pouvez fermer la fenêtre';
		$('#save_params').attr('disabled', 'disabled');
	}
	else {
		retImg = 'error';
		retMsg = _retMsg;
	}

	$('#loader').addClass('hidden');
	state.attr('src', '../assets/' + retImg + '.png');
	state.show();

	$('#msg_save').html(retMsg);
	setTimeout(() => {
		state.hide();
		$('#msg_save').html('');
		$('#save_params').removeAttr('disabled');
	}, 5000);
	
});
$('#cancel').on('click', () => {
	var win = remote.getCurrentWindow();
	win.close();
});

/** 
* updateGrid()
* Met à jour les paramètres
*/
function updateParams() {
	// demande de la liste
	ipc.send('get_params');
	
}

ipc.on('return_get_params', (event, api_key, refresh, currenciesList, startCurrency) => {
	var currencies = JSON.parse(currenciesList);
	$('#api_key').empty().val(api_key);
	$('#refresh').empty().val(refresh);
	$('#current_start_currency').empty().html(startCurrency);
	
	var select = $("#select_start_currency");
	select.empty();
	
	for(var curr in currencies) {	// curr => clé = Code ISO
		var opt = document.createElement('option');
		opt.value = curr;
		opt.innerHTML = curr + " (" + currencies[curr][CURRENCY_NAME] + ")";
		
		opt.selected = (curr == startCurrency)? "selected" : "";
		select.append(opt);
	}
});