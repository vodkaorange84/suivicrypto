const ipc = require('electron').ipcRenderer;
const remote = require('electron').remote;
window.$ = window.jquery = require('jquery');
require('bootstrap');

// fonctions en provenance du main process
var isExistIcon = remote.require('./main').isExistIcon;

var refreshValue;
var refreshTo = null, displayInterval = null;

var pauseState = false;	// état de la pause (true = pause active - false : pause inactive)

// Objets du DOM
var list = $("#grid_currencies");
var spin = $('#loader');
var select = $("#select_start_currency");

var refreshBtn = $('#refresh-button');
var stopBtn = $('#stop_update');

var cryptoOnly = $('#crypto_only');

var nextUpdate = $('#next_update');
var stateUpdate = $('#state_update');

var update = $('#update_div');

// Récupère la liste des monnaies de départ
ipc.send('get_interval');
ipc.on('return_get_interval', (event, _interval) => {
	refreshValue = _interval;

	stopBtn.text('Arrêter');
	stopBtn.attr('title', 'Arrêter le compte à rebours');
	stateUpdate.removeClass().addClass('fa fa-play-circle');

	startOrStopRefresh(true, false);
});


// détection du changement de monnaie
select.on('change', function() {
	ipc.send('change_start_currency', this.value);
	
	startOrStopRefresh(true, false);
});

// appui sur le bouton de rafraichissement des données
refreshBtn.on('click', function() {
	startOrStopRefresh(true, true);
});

// détection de la checkbox Cryptomonnaies uniquement
cryptoOnly.on('change', function() {
	startOrStopRefresh(true, true);
});

// Appui sur le bouton Stop / Reprendre
stopBtn.on('click', () => {
	if(pauseState) {		// on est en pause, on reprend
		stopBtn.text('Arrêter');
		stopBtn.attr('title', 'Arrêter le compte à rebours');
		$('.left').removeClass('left-stop');
		stateUpdate.removeClass().addClass('fa fa-play-circle');

		startOrStopRefresh(true, false);
	}
	else {					// on met en pause
		stopBtn.text("Redémarrer");
		stopBtn.attr('title', 'Redémarre le compte à rebours à ' + refreshValue + ' secondes');
		update.addClass('left-stop');
		stateUpdate.removeClass().addClass('fa fa-pause-circle');

		startOrStopRefresh(false, true);
	}
	pauseState = !pauseState;
});

/** 
* startOrStopRefresh()
* Initialise ou réinitialise le délai de rafraichissement
*/
function startOrStopRefresh(start, stop) {
	if(stop) {
		clearTimeout(refreshTo);
		clearInterval(displayInterval);
		nextUpdate.empty();
		if(!start) { return; }
	}

	if(start) {
		updateGrid();
		// On démarre un intervalle de temps pour l'affichage
		var dest = Date.now() + (refreshValue * 1000);	// destination
		if(displayInterval != null) {
			clearInterval(displayInterval);
		}
		displayInterval = setInterval(() => {
			nextUpdate.removeClass('hidden');
			var now = Date.now();
			var dist = Math.floor((dest - now) / 1000);
			nextUpdate.empty().html(dist + ' secondes');

			if(dist <= 0) {
				clearInterval(displayInterval);
				nextUpdate.html('Mise à jour ...');//.addClass('hidden');
			}
		}, 1000);	// mise à jour du compteur toutes les secondes


		// Définition du timeout pour le prochain cycle
		refreshTo = setTimeout(() => {
 			startOrStopRefresh(start, stop);
		}, refreshValue * 1000);
	}
}


/** 
* updateGrid()
* Met à jour la liste des devises (envoi d'un message au main)
*/

function updateGrid() {
	spin.removeClass('hidden');
	ipc.send('get_currencies', {
		values: true,
		crypto_only: cryptoOnly.prop('checked')
	});
}
// Retour de la demande de la liste des devises
ipc.on('return_get_currencies', (event, currenciesList, ts, startCurrency) => {	// startCurrency => code ISO (en provenance de settings)
	select.empty();
	list.empty();

	var d = new Date(ts);
	$('#update_date').html(zeroFormat(d.getDate()) + '/' + zeroFormat(d.getMonth() + 1) + '/' + zeroFormat(d.getFullYear()));
	$('#update_hour').html(zeroFormat(d.getHours()) + ':' + zeroFormat(d.getMinutes()) + ':' + zeroFormat(d.getSeconds()));

	var currencies = JSON.parse(currenciesList);
	var max = 0, value;
	var label;
	var classLi = '';
	var li, opt, icon;
	var num = new Intl.NumberFormat('fr-FR');
	for(var curr in currencies) {	// curr => clé = Code ISO
		classLi = (curr == startCurrency)? 'class="selected_currency"' : '';	// sélectionne la monnaie en cours

		label = curr + ' (' + shortenStr(currencies[curr][CURRENCY_NAME]) + ')';
		max = (label.length > max)? label.length : max;		// Calcul de la taille du label le plus grand

		// une icone existe-t-elle pour cette monnaie ?
		icon = (isExistIcon(curr.toLowerCase()))? '<img src="../assets/currencies/' + curr.toLowerCase() + '.png" alt="" class="currency-icon" />' : '<span class="no-icon"></span>';

		// Gestion de l'affichage de la valeur
		value = (currencies[curr][RATE_CHANGE] > 0.01)? num.format(currencies[curr][RATE_CHANGE]) : currencies[curr][RATE_CHANGE].toString().replace(".", ","); 

		// Construction de la ligne pour affichage
		li = '<li ' + classLi + '>' 
				+ '<label class="label-currency">' + icon + ' ' + curr + ' (<span title="' + currencies[curr][CURRENCY_NAME] + '">' + shortenStr(currencies[curr][CURRENCY_NAME]) + '</span>)</label> ' 
				+ '<span>= ' + value + ' ' + curr + '</span> '
			+ '</li>';

		list.append(li);

		// création de la liste déroulante
		opt = document.createElement('option');
		opt.value = curr;
		opt.innerHTML = curr + " (" + currencies[curr][CURRENCY_NAME] + ")";
		
		opt.selected = (curr == startCurrency)? "selected" : "";
		select.append(opt);
	}
	$('label.label-currency').css('width', (max + 10) + 'vw');	// recalcul du la largeur maximal pour le label (alignement des valeurs)

	spin.addClass('hidden');
});

/**
* zeroFormat()
* Ajoute un 0 devant les chiffres (pour 0 => 00 à 9 => 09), pour l'affichage
* @param {Integer} value Valeur à traiter
* @return {String} Valeur avec 0 devant (si < 10) ou la valeur elle même (si > 10)
*/
function zeroFormat(value) {
  return (value < 10)? '0' + value : value;
}
/**
* shortenStr()
* Raccourci une chaine de caractère (typiquement pour le nom des devises)
* @param {String} _string chaine de caractère
* @param {String} pattern pattern pour le milieu ([...] par défaut)
* @param {Integer} trigger Longueur de déclenchement (20 par défaut)
* @param {Integer} lastSize Longueur à conserver pour la seconde partie (11 par défaut)
* @return {String} Chaine de caractère raccourci (si > à la longueur), la chaine initiale sinon
*/
function shortenStr(_string, pattern = '[...]', trigger = 20, lastSize = 11) {
  var first = '', last = '';
  if(_string.length > trigger) {
    first = _string.substring(0, _string.indexOf(" ")).trim();
    last = _string.substring((_string.length - lastSize)).trim();
    return first + ' ' + pattern + ' ' + last;
  }
  return _string;
}