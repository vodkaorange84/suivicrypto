const electron = require('electron');

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const Tray = electron.Tray;

const globalShortcut = electron.globalShortcut;


const ipc = electron.ipcMain;	// permet de recevoir les flux ipc côté script principal
const remote = require('electron').remote;		// remote pour accéder aux objets graphiques, à l'OS, ... non accessible par la partie rendu


const request = require('request');
const dialog = require('electron').dialog;
const shell = require ('electron').shell;
const fs = require('fs');


// SQLite
const Database = require('better-sqlite3');
const db = new Database('bdd');

// Constantes
const CURRENCY_NAME	= 0;
const IS_CRYPTO 	= 1;
const RATE_CHANGE 	= 2;

const ICON = 'assets/icon.png';		// icone de l'application

// Bibliothèques
let $ = require('jquery');

// Fonctions base de données
/**
* getCurrencies()
* Retourne la liste des devises enregistrées dans l'application 
* @return {Array} tableau des devises dont l'index est le code ISO
*/
function getCurrencies() {
	let query = db.prepare('SELECT c.iso_code, cn.name, cn.is_crypto FROM currencies c INNER JOIN currencies_name cn ON c.iso_code = cn.iso_code');
	let rows = query.all();
	var cur = {};
	rows.forEach((element) => {
		cur[element.iso_code] = [element.name, element.is_crypto, 0];
	});
	return cur;
}

/**
* getSettings()
* Retourne les paramètres de l'application
* @return {Array} tableau contenant les paramètres : [variable] = valeur
*/
function getSettings() {
	let query = db.prepare("SELECT * FROM settings");
	let rows = query.all();
	var sett = [];
	rows.forEach((element) => {
		sett[element.variable] = element.value;
	});
	return sett;
}
/**
* saveParam()
* Sauvegarde la valeur du paramètre
* @param {String} _param Paramètre à enregistrer
* @param {String} _value Valeur du paramètre
* @return {Boolean} Etat de l'enregistrement dans la table
*/
function saveParam(_param, _value) {
	if(settings[_param] != 'undefined') {
		var stm = db.prepare("UPDATE settings SET value = ? WHERE variable = ?");
		var res = stm.run(_value, _param);
		return (res.changes == 1)? true : false;
	}
	return false;
}
/**
* getParam()
* Retourne la valeur du paramètre passé en argument
* @param {String} _param Paramètre à lire (doit exister en BDD, retourne false sinon)
* @return {String} Valeur du paramètre
*/
function getParam(_param) {
	if(settings[_param] != 'undefined') {
		let query = db.prepare('SELECT value FROM settings WHERE variable = ?');
		let value = query.get(_param);
		return value['value'];
	}
	return false;
}
/**
* isIsoExist()
* Indique si le code ISO passé en argument existe dans la table currencies_name
* @param {String} iso_code code ISO à chercher
* @return {Boolean} Existe (= true) ou non (= false)
*/
function isIsoExist(iso_code) {
	let query = db.prepare('SELECT COUNT(*) AS nb FROM currencies_name WHERE iso_code = ?');
	let value = query.get(iso_code);
	return (parseInt(value['nb']) > 0)? true : false;
}
/**
* addIsoCode()
* Insère le code ISO dans la table currencies (ajoute la monnaie à la liste des devises)
* @param {String} iso_code code ISO à enregistrer
* @return {Boolean} Etat de l'enregistrement dans la table
*/
function addIsoCode(iso_code) {
	let query = db.prepare('INSERT OR IGNORE INTO currencies(iso_code) VALUES(?)');
	let value = query.run(iso_code);
	return (value.changes == 1)? true : false;
}
/**
* removeIsoCode()
* Supprime le code ISO de la table currencies (retire la monnaie de la liste des devises)
* @param {String} iso_code code ISO à supprimer
* @return {Boolean} Etat de la suppression dans la table
*/
function removeIsoCode(iso_code) {
	let query = db.prepare('DELETE FROM currencies WHERE iso_code = ?');
	let value = query.run(iso_code);
	return (value.changes == 1)? true : false;
}

/**
* isExistIcon()
* Indique si le fichier de la monnaie existe dans le dossier assets
* @param {String} icon nom du fichier sans l'extension
* @param {String} ext extension du fichier (png par défaut)
* @return {Boolean} Le fichier existe (= true) ou non (= false)
*/
function isExistIcon(icon, ext = 'png') {
	var folder = 'assets/currencies/';
	return fs.existsSync(folder + icon + '.' + ext);
}
exports.isExistIcon = isExistIcon;

/**
* getVersion()
* Retourne la version actuelle du logiciel
* @return {String} Version lue dans le fichier package.json
*/
function getVersion() {
	return app.getVersion();
}
exports.getVersion = getVersion;

// Lecture des devises de l'applciation
let currencies = getCurrencies();

// Paramètres de l'application
let settings = getSettings();

// objets des fenêtres
let mainWindow, aboutWindow, manageWindow, settingWindow;

// Variables
let returnCurrencies;

// Définition de la fenêtre principale
function createMainWindow() {
	mainWindow = new BrowserWindow({
		width: 660, 
		height: 485,
		minWidth: 660,
		minHeight: 485,
		title: "Suivi des cryptomonnaies",
		icon: ICON,
		show: false, 
		resizable: true
	}); 

	mainWindow.loadURL(`file://${__dirname}/src/index.html`);
	mainWindow.once('ready-to-show', () => {
		mainWindow.show();
	});
	mainWindow.on('closed', () => {
		mainWindow = null;
	});

	// définition du menu principal
	const template = [
		{
			label: 'Fichier', 
			submenu: [ {
					label: 'Gérer les devises', 
					click: () => {
						createManageWindow();
					}
				}, {
					label: 'Paramètres',
					click: () => {
						createSettingWindow();
					}
				}, {
					type: 'separator'
				}, {
					label: 'Quitter', 
					click: () => {
						app.quit();
					}
				}
			]
		}, 
		{
			label: '?',
			submenu: [
				{
					label: 'A propos', 
					click: () => {
						createAboutWindow();
					}
				}
			]
		}
	];
	const menu = Menu.buildFromTemplate(template);
	Menu.setApplicationMenu(menu);

	// définition de l'icone dans la barre de notification
	const tray = new Tray('assets/icon.png');
	tray.setToolTip('Suivi des cours');
	tray.setContextMenu(Menu.buildFromTemplate([
		{
			label: 'Rafraichir',
			click: () => {
				console.log("Rafraichissement des cours sur l'appli");
			}
		}
	]));
}

// Définition de la fenêtre A propos
function createAboutWindow() {
	aboutWindow = new BrowserWindow({
		parent: mainWindow,	
		title: 'A propos', 
		width: 660,
		height: 480,
		modal: true, 
		show: false, 
		icon: ICON,
		resizable: false
	});
	aboutWindow.setMenu(null);
	aboutWindow.loadURL(`file://${__dirname}/src/about.html`);
	aboutWindow.once('ready-to-show', () => {
		aboutWindow.show();
	});
	aboutWindow.on('closed', () => {
		aboutWindow = null;
	});
}

// Définition de la fenêtre Gestion des devises
function createManageWindow() {
	manageWindow = new BrowserWindow({
		parent: mainWindow,	
		title: 'Gestion des devises', 
		width: 500,
		height: 350,
		modal: true, 
		show: false, 
		icon: ICON,
		resizable: false
	});
	manageWindow.setMenu(null);
	manageWindow.loadURL(`file://${__dirname}/src/manage.html`);
	manageWindow.once('ready-to-show', () => {
		manageWindow.show();
	});
	manageWindow.on('closed', () => {
		manageWindow = null;
	});
}

// Définition de la fenêtre Paramètres
function createSettingWindow() {
	settingWindow = new BrowserWindow({
		parent: mainWindow,
		title: 'Paramètres',
		width: 555,
		height: 280,
		modal: true, 
		show: false, 
		icon: ICON,
		resizable: true
	});
	settingWindow.setMenu(null);
	settingWindow.loadURL(`file://${__dirname}/src/setting.html`);
	settingWindow.once('ready-to-show', () => {
		settingWindow.show();
	});
	settingWindow.on('closed', () => {
		settingWindow = null;
	});
}

// lance la fenetre lorsque Electron a finit de se charger
app.on('ready', () => {
	const ret = globalShortcut.register('CommandOrControl+Shift+I', () => {
		if(aboutWindow != null) {
			aboutWindow.webContents.openDevTools();
		}
		else if(manageWindow != null) {
			manageWindow.webContents.openDevTools();
		}
		else if(settingWindow != null) {
			settingWindow.webContents.openDevTools();
		}
		else {
			mainWindow.webContents.openDevTools();
		}
	});

	if(settings['api_key'] == '') {
		dialog.showMessageBox(
			{
				type: 'warning',
				title: 'Clé API', 
				message: "Attention, la clé API Min Compare peut être nécessaire. L'application peut fonctionner sans mais en cas d'erreur, vous devrez en créer une."
				+ "Pour cela, vous trouverez le lien du site dans la fenêtre A Propos",
				buttons: ['OK']
			},
			(response) => {
				createMainWindow();
			}
		);
	}
	else {
		createMainWindow();
	}
});


// Quitte l'application lorsque toutes les fenetres sont fermées
app.on('window-all-closed', () => {
	if(process.platform !== 'darwin') {	// condition pour MacOS
		app.quit();
	}
});

app.on('activate', () => {
	if(mainWindow === null) {
		createMainWindow();
	}
	
});
app.on('will-quit', () => {
	globalShortcut.unregister('CommandOrControl+X');
});

// Interaction Main <=> Browser via ipc

// [Main] Modification de la monnaie de départ et mise à jour
ipc.on('change_start_currency', (event, args) => {
	settings['startCurrency'] = args;		// iso_code
});

// [Main et Gestion des devises] Liste des devises enregistrées sur l'appli
ipc.on('get_currencies', (event, args) => {
	var ts = -1;
	var requestCurrencies = {};		// tableau temporaire permettant de stocker les devises à récupérer
	if(args['crypto_only']) {
		for(var curr in currencies) {
			if(currencies[curr][IS_CRYPTO] == 1) {
				requestCurrencies[curr] = currencies[curr];
			}
		}
	}
	else {
		requestCurrencies = currencies;
	}


	if(args['values']) {		// MAJ du cours des monnaies, en provenance de l'API

		var cur = Object.keys(requestCurrencies);	// la clé étant le code ISO, récupère les clés du tableau
		var url = "https://min-api.cryptocompare.com/data/price?fsym=" + settings['startCurrency'] + "&tsyms=" + cur.join(',') + "&api_key=" + settings['api_key'];
		console.log(url);
		
		request({url: url}, (err, res, body) => {
			if(err) {
				return console.log("error : " + err);
			}
			try {
				var rawParse = JSON.parse(body);
				ts = Date.now();
				if(!rawParse.hasOwnProperty('Response')) {		// pas d'erreur du flux JSON, on traite
					// récupère les valeurs des devises selon le code ISO de la monnaie de départ puis assignation
					for(var i in rawParse) {
						requestCurrencies[i][RATE_CHANGE] = rawParse[i];
					}
					returnCurrencies = JSON.stringify(requestCurrencies);
					console.log("Mise à jour : " + new Date(ts));
				}
				else {	// problème

				}
				event.sender.send('return_get_currencies', returnCurrencies, ts, settings['startCurrency']);
				
			}
			catch(e) {
				return console.log("error : " + e);
			}
		});
	}
	else {		// on ne veut que les code ISO et leurs noms
		returnCurrencies = JSON.stringify(requestCurrencies);
		
	}

	event.sender.send('return_get_currencies', returnCurrencies, ts, settings['startCurrency']);

	
});

// [Main] Récupère le délai en secondes
ipc.on('get_interval', (event, args) => {
	event.sender.send('return_get_interval', settings['refresh']);
});

// [Gestion des devises] gestion de l'ajout d'une monnaie sur l'appli
ipc.on('add_currency', (event, args) => {
	let result = '2', error = '';
	args = args['iso_code'].trim();
	if(args) {		// la valeur est définie
		if(isIsoExist(args)) {	// le code ISO existe dans la BDD ?
			if(addIsoCode(args)) {
				currencies = getCurrencies();	// refresh du tableau
				result = '0';
			}
			else {
				result = '-1';
				error = "Erreur lors de l'enregistrement";	
			}
		}
		else {
			result = '-2';
			error = "Ce n'est pas un code ISO !";
		}
	}
	else {
		result = '-3';
		error = "La valeur n'est pas définie";
	}
	event.sender.send('return_add_currency', result, error);
});

// [Gestion des devises] Gestion de la suppression
ipc.on('delete_currency', (event, args) => {
	let result = (removeIsoCode(args['iso_code']))? '1' : '-1';
	currencies = getCurrencies();
	if(Object.keys(currencies).indexOf(settings['startCurrency']) == -1) {	// si on supprime la devise de départ
		settings['startCurrency'] = Object.keys(currencies)[0];
	}
	event.sender.send('return_delete_currency', result);
});

// [Paramètres] Récupère les paramètres
ipc.on('get_params', (event, args) => {
	var currenciesList = JSON.stringify(currencies);
	event.sender.send('return_get_params', settings['api_key'], settings['refresh'], currenciesList, settings['startCurrency']);
});

// [Paramètres] Sauvegarde des paramètres en BDD
ipc.on('save_params', (event, args) => {
	settings['api_key'] = args['api_key'];
	settings['refresh'] = args['refresh'];
	settings['startCurrency'] = args['select_start_currency'];

	// traitement de l'enregistrement
	var ret = true;
	var retMsg = "";
	if(!saveParam('api_key', settings['api_key']) || !saveParam('refresh', settings['refresh']) || !saveParam('startCurrency', settings['startCurrency'])) {
		ret = true;
		retMsg = "Erreur de sauvegarde";
	}

	event.sender.send('return_save_params', ret, retMsg);
});

// [A propos] Ouvre le lien cliqué
ipc.on('open_link', (event, args) => {
	console.log('url : ' + args['url']);
	shell.openExternal(args['url']);
});